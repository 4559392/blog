/// <reference path="init.ts" />
/// <reference path="index.ts" />
/// <reference path="post.ts" />

(function () {
    const imgs = document.getElementsByTagName("img")
    for (let i = 0; i < imgs.length; i++) {
        const img = imgs[i]
        let s = img.src
        if (s.length > 25) {
            s = s.substring(0, 25) + "..."
        }
        img.alt = "图片加载失败 " + s
    }
})();

(function () {
    const links = document.getElementsByTagName("a")
    const myHost = location.host
    for (let i = 0; i < links.length; i++) {
        const a = links[i]
        let jumpopen = false
        if (a.host.length > 0 && a.host != myHost) {
            jumpopen = true
        } else {
            let p = a.pathname.toLowerCase()
            if (p.endsWith(".js") || p.endsWith(".xml")) {
                jumpopen = true
            }
        }
        if (jumpopen) {
            a.target = "_blank"
        }
    }
})();

(function () {
    const es = document.getElementsByTagName("time")
    for (let i = 0; i < es.length; i++) {
        const tm = es[i] as HTMLTimeElement
        const dt = new Date(tm.dateTime)
        if (dt.getFullYear() < 2017) {
            tm.style.display = None
        }
    }
})();

