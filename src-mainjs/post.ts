/// <reference path="init.ts" />

(function () {
    const articleFull = document.getElementsByClassName("articleFull")[0]
    if (articleFull == null) {
        return
    }
    const divcontent = document.getElementsByClassName("articleContent")[0] as HTMLDivElement
    const codelist = divcontent.querySelectorAll("code")
    if (codelist.length > 0) {
        const js = document.createElement("script")
        js.src = "/highlight.min.js"
        js.addEventListener("load", function () {
            hljs.highlightAll()
        })
        document.body.appendChild(js)
        const ShowCode = "显示代码"
        const HideCode = "隐藏代码"
        const CopyCode = "复制代码"
        const cantCopy = navigator.clipboard == null
        const AfterCopyCode = "复制成功！"
        const CantCopyCode = "代码复制失败"
        for (const codeElement of codelist) {
            const parent = codeElement.parentElement
            if (parent == null) {
                throw " <code> 的父元素是 null"
            }
            if (parent.tagName.toUpperCase() == 'PRE') {
                const pre = parent as HTMLPreElement
                pre.className += ' hljs'
                const txt = pre.innerText
                const divBefore = document.createElement("div")
                divBefore.className = "codeBefore"
                const butCopy = document.createElement("button")
                butCopy.innerText = CopyCode
                let lastTimer = 0
                butCopy.addEventListener("click", function () {
                    if (cantCopy) {
                        butCopy.innerText = CantCopyCode
                        return
                    }
                    navigator.clipboard.writeText(txt)
                    butCopy.innerText = AfterCopyCode
                    if (lastTimer != 0) {
                        clearTimeout(lastTimer)
                    }
                    lastTimer = setTimeout(function () {
                        butCopy.innerText = CopyCode
                        lastTimer = 0
                    }, 1300)
                })
                divBefore.appendChild(butCopy)
                if (txt.length > 500 || txt.split("\n").length > 9) {
                    const divPreview = document.createElement("code")
                    divPreview.className = 'hljs'
                    divPreview.innerText = txt.substring(0, 15).trim().replaceAll(/[\n\r]+/gim, " ") + " ......"
                    const butHide = document.createElement("button")
                    butHide.innerText = ShowCode
                    butHide.addEventListener("click", function () {
                        if (pre.style.display == None) {
                            pre.style.display = Block
                            butHide.innerText = HideCode
                            divPreview.style.display = None
                        } else {
                            pre.style.display = None
                            butHide.innerText = ShowCode
                            divPreview.style.display = Block
                        }
                    })
                    butHide.dispatchEvent(new Event("click"))
                    divBefore.appendChild(butHide)
                    divBefore.appendChild(divPreview)
                }
                const outbox = pre.parentElement as HTMLDivElement
                outbox.insertBefore(divBefore, pre)
            } else {
                codeElement.className += ' hljs'
            }
        }
    }
})();
