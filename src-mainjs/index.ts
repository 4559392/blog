/// <reference path="init.ts" />

(function () {
    const divSort = document.createElement("div")
    divSort.style.marginTop = "10px"
    const labSort = document.createElement("span")
    labSort.innerText = ""
    divSort.appendChild(labSort)
    const butViewAll = document.createElement("a")
    butViewAll.innerText = "取消筛选"
    butViewAll.href = "/#"
    butViewAll.style.marginLeft = '20px'
    divSort.appendChild(butViewAll)
    mainBody.insertBefore(divSort, mainBody.firstChild)

    const sortTags = function () {
        const t = decodeURIComponent(location.hash.toLowerCase().replaceAll("#", ""))
        const viewAll = t.length < 1
        divSort.style.display = viewAll ? None : Block
        labSort.innerText = "筛选标签：  " + t
        window.scroll(0, 0)
        const posts = mainBody.getElementsByClassName("articleBox")
        const tMark = "," + t + ","
        for (let i = 0; i < posts.length; i++) {
            const div = posts[i] as HTMLDivElement
            if (viewAll) {
                div.style.display = Block
            } else {
                const tags = div.getAttribute("data-tags")
                if (tags == null) {
                    continue
                }
                div.style.display = tags.includes(tMark) ? Block : None
            }
        }
    }

    window.addEventListener("hashchange", sortTags)
    sortTags()
})();
