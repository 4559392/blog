# 陈布衣的博客生成器
Go 语言写的，除此之外，没什么奇特的。   

命令行参数：   
```bash
# 启动本地预览服务器，默认端口是 22233
./bgg.ps1 s
./bgg.ps1 s <port>

# 生成全部文件到 /public 文件夹
./bgg.ps1 g

# 新建普通的 markdown 文章
./bgg.ps1 new <name>

# 新建 html 文章 
./bgg.ps1 newhtml <name>
```

使用的第三方库：
- [gomarkdown/markdown](https://github.com/gomarkdown/markdown)
- [highlight.js](https://highlightjs.org/)
- [@types/tampermonkey](https://www.npmjs.com/package/@types/tampermonkey)

