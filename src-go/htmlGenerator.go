package main

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"text/template"
)

var topTmpl *template.Template
var notFoundTmpl *template.Template
var indexTmpl *template.Template
var postTmpl *template.Template
var atomTmpl *template.Template

type TopHtmlInfo struct {
	Title string
	Body  string
}

// 从 /html 里面读取指定的模板
func loadHtmlTemplateFiles() error {
	readFile := func(name string, ptr **template.Template) error {
		bs, updated, err := ReadFileOrCache(filepath.Join(fdHtml, name+".html"))
		if err != nil {
			return fmt.Errorf("%v %v", name, err)
		}
		if updated {
			t := template.New("")
			t, err = t.Parse(string(bs))
			if err != nil {
				return fmt.Errorf("%v %v", name, err)
			}
			*ptr = t
			return nil
		}
		return nil
	}
	var err error
	err = readFile("top", &topTmpl)
	if err != nil {
		return err
	}
	err = readFile("404", &notFoundTmpl)
	if err != nil {
		return err
	}
	err = readFile("index", &indexTmpl)
	if err != nil {
		return err
	}
	err = readFile("post", &postTmpl)
	if err != nil {
		return err
	}
	err = readFile("atom", &atomTmpl)
	if err != nil {
		return err
	}
	return nil
}

var postsCache = make(map[string]*Post)

// 从 /posts 里面读取全部文章，只能读取 index.html 或者 *.md ，输出一个包含全部的文章并排序后的数组
func loadPostFiles() ([]*Post, error) {
	usedFileTitles := make([]string, 0)
	var fc = func(path string, d os.DirEntry, err error) error {
		if err != nil {
			return err
		}
		if d.IsDir() {
			return nil
		}
		ext := filepath.Ext(path)
		if strings.Contains(path, "node_modules") || (ext != ".md" && !strings.HasSuffix(path, "index.html")) {
			return nil
		}
		ft := ParsePostFileTitleFromPath(path)
		if len(ft) < 1 {
			return fmt.Errorf("文章文件名为空 %v", path)
		}
		if IsStringInArray(usedFileTitles, ft, true) {
			return fmt.Errorf("文章文件名重复 %v", path)
		}
		content, updated, err := ReadFileOrCache(path)
		if err != nil {
			return err
		}
		if updated {
			var p *Post
			stream := strings.NewReader(string(content))
			p, err = ParsePostFromStream(ft, stream)
			if err != nil {
				return err
			}
			postsCache[ft] = p
		}
		usedFileTitles = append(usedFileTitles, ft)
		return nil
	}
	// var catchErr = func(path string, d os.DirEntry, err error) error {
	// 	e := fc(path, d, err)
	// 	if e != nil {
	// 		fmt.Printf("文章扫描出错，跳过： %s %v\n", path, e)
	// 	}
	// 	return nil
	// }
	err := filepath.WalkDir(fdPosts, fc)
	if err != nil {
		return nil, err
	}
	size := len(usedFileTitles)
	if size < 1 {
		return nil, fmt.Errorf("posts 文件夹里一篇文章都没有")
	}
	for t := range postsCache {
		if !IsStringInArray(usedFileTitles, t, true) {
			delete(postsCache, t)
		}
	}
	out := make(SortPostsByDate, 0)
	for _, p := range postsCache {
		if p != nil {
			out = append(out, p)
		}
	}
	sort.Sort(out)
	return out, nil
}

// 生成 atom 的 xml 文档，输出到流里
func generateAtomXML(output io.Writer, posts []*Post) error {
	err := atomTmpl.Execute(output, posts)
	return err
}

// 生成整个页面，输出到流里
func generatePage(output io.Writer, title string, tmpl *template.Template, bodyData interface{}) error {
	buffer := new(bytes.Buffer)
	err := tmpl.Execute(buffer, bodyData)
	if err != nil {
		return err
	}
	info := &TopHtmlInfo{Title: title, Body: buffer.String()}
	err = topTmpl.Execute(output, info)
	return err
}

// 生成全部的文件，输出到 /public 文件夹
func buildAllFiles() error {
	var err error
	err = os.RemoveAll(fdPublic)
	if err != nil {
		return fmt.Errorf("重建 public 文件夹出错 %v", err)
	}
	err = os.MkdirAll(fdPublic, os.ModePerm)
	if err != nil {
		return fmt.Errorf("创建 public 文件夹出错 %v", err)
	}
	count, err := CopyFolder(fdStatics, fdPublic, true)
	if err != nil {
		return fmt.Errorf("复制 statics 文件夹出错 %v", err)
	}
	fmt.Println("复制 statics 文件夹完毕，个数：", count)
	posts, err := loadPostFiles()
	if err != nil {
		return fmt.Errorf("扫描 /posts 文章列表出错 %v", err)
	}
	err = loadHtmlTemplateFiles()
	if err != nil {
		return fmt.Errorf("扫描 /html 模板出错 %v", err)
	}
	writeHTML := func(path string, title string, tmpl *template.Template, bodydata interface{}) error {
		stream, err := os.Create(path)
		if err != nil {
			return err
		}
		defer stream.Close()
		return generatePage(stream, title, tmpl, bodydata)
	}
	path := ""
	for _, post := range posts {
		path = filepath.Join(fdPublic, post.Filetitle+".html")
		err = writeHTML(path, post.Title+" - ", postTmpl, post)
		if err != nil {
			return fmt.Errorf("生成文章页出错 %v %v", post.Filetitle, err)
		}
		fmt.Println("生成完毕：", path)
	}
	path = filepath.Join(fdPublic, "atom.xml")
	atomStream, err := os.Create(path)
	if err != nil {
		return fmt.Errorf("生成 atom 出错 %v", err)
	}
	defer atomStream.Close()
	err = generateAtomXML(atomStream, posts)
	if err != nil {
		return fmt.Errorf("生成 atom 出错 %v", err)
	}
	fmt.Println("生成完毕：", path)
	path = filepath.Join(fdPublic, "404.html")
	err = writeHTML(path, "404 - ", notFoundTmpl, nil)
	if err != nil {
		return fmt.Errorf("生成 404 页出错 %v", err)
	}
	fmt.Println("生成完毕：", path)
	path = filepath.Join(fdPublic, "index.html")
	err = writeHTML(path, "", indexTmpl, posts)
	if err != nil {
		return fmt.Errorf("生成 index 页出错 %v", err)
	}
	fmt.Println("生成完毕：", path)
	return nil
}

// 处理 HTTP 请求
func handleHttpRequest(rw http.ResponseWriter, r *http.Request) {
	var err error
	fmt.Printf("%v\t%v\n", r.Method, r.URL)
	if r.Method != "GET" {
		rw.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
	url := strings.Trim(r.URL.Path, "/")
	writeError := func(msg interface{}) {
		fmt.Println(msg)
		rw.WriteHeader(http.StatusInternalServerError)
		rw.Write([]byte(fmt.Sprintf("HTTP 500\n%v\n%v", url, msg)))
	}
	var posts []*Post
	err = loadHtmlTemplateFiles()
	if err != nil {
		writeError(fmt.Errorf("读取 /html 模板出错 %v", err))
		return
	}
	posts, err = loadPostFiles()
	if err != nil {
		writeError(fmt.Errorf("读取 /posts 出错 %v", err))
		return
	}
	if len(url) < 1 || url == "index.html" {
		err = generatePage(rw, "", indexTmpl, posts)
		if err != nil {
			writeError(err)
		}
		return
	}
	url = strings.TrimSuffix(strings.TrimSuffix(url, ".html"), "/index.html")
	for _, p := range posts {
		if p.Filetitle == url {
			err = generatePage(rw, p.Title+" - ", postTmpl, p)
			if err != nil {
				writeError(err)
			}
			return
		}
	}
	if url == "atom.xml" {
		err = generateAtomXML(rw, posts)
		if err != nil {
			writeError(err)
		}
		return
	}
	path := filepath.Join(fdStatics, url)
	bs, _, err := ReadFileOrCache(path)
	if err != nil && !os.IsNotExist(err) {
		writeError(err)
		return
	}
	if bs != nil {
		tp := ""
		switch filepath.Ext(url) {
		case ".js":
			tp = "application/javascript; charset=utf-8"
		case ".css":
			tp = "text/css; charset=utf-8"
		case ".svg":
			tp = "image/svg+xml; charset=utf-8"
		}
		if len(tp) > 0 {
			rw.Header().Add("Content-Type", tp)
		}
		rw.Write(bs)
		return
	}
	rw.WriteHeader(http.StatusNotFound)
	err = generatePage(rw, "404 - ", notFoundTmpl, nil)
	if err != nil {
		writeError(err)
		return
	}
}
