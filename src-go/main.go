package main

import (
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"text/template"
	"time"
)

var (
	fdWorkingRoot string = ""
	fdStatics     string = "statics"
	fdPosts       string = "posts"
	fdHtml        string = "html"
	fdPublic      string = "public"
)

func main() {
	fmt.Println("陈布衣的博客生成器")
	locateWorkingDirectory()
	args := os.Args
	act := "s"
	actValue := ""
	argslen := len(args)
	if argslen > 1 {
		act = strings.ToLower(args[1])
	}
	hasActValue := false
	if argslen > 2 {
		actValue = args[2]
		hasActValue = true
	}
	switch act {
	case "s":
		port := 22233
		if hasActValue {
			num, err := strconv.ParseInt(actValue, 10, 32)
			if err != nil || num > 65535 || num < 80 {
				panic("给定的端口号无法识别")
			}
			port = int(num)
		}
		startWebServer(port)
	case "g":
		buildAllFiles()
		fmt.Println("工作结束")
	case "new":
		if !hasActValue {
			panic("没有给定一个可用的文件名")
		}
		createNewPost(strings.ToLower(actValue), false)
	case "newhtml":
		if !hasActValue {
			panic("没有给定一个可用的文件名")
		}
		createNewPost(strings.ToLower(actValue), true)
	default:
		panic("指令无法被识别，请看 README.md")
	}
}

// 尝试确定软件工作的文件夹
func locateWorkingDirectory() {
	if len(fdWorkingRoot) > 0 {
		panic("重复调用了 locateWorkingDirectory()")
	}
	fmt.Println("正在尝试定位软件的工作文件夹")
	wd, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	fdWorkingRoot = wd
	needed := []string{fdStatics, fdHtml, fdPosts}
	needcount := len(needed)
	const maxEntryCount = 15
	for retry := 0; retry < 10; retry++ {
		fmt.Println("检查：", wd)
		entrys, err := os.ReadDir(wd)
		if err != nil {
			panic(err)
		}
		found := 0
		entrycount := len(entrys)
		if entrycount > maxEntryCount {
			fmt.Printf("↑此文件夹子项目超过 %d 个，跳过。\n", maxEntryCount)
		} else {
			for i := 0; i < entrycount; i++ {
				fd := entrys[i]
				if fd.IsDir() {
					for j := 0; j < len(needed); j++ {
						if strings.EqualFold(fd.Name(), needed[j]) {
							found += 1
							break
						}
					}
				}
				if found >= needcount {
					os.Chdir(wd)
					fdWorkingRoot = wd
					fdPublic = filepath.Join(fdWorkingRoot, fdPublic)
					fdHtml = filepath.Join(fdWorkingRoot, fdHtml)
					fdPosts = filepath.Join(fdWorkingRoot, fdPosts)
					fdStatics = filepath.Join(fdWorkingRoot, fdStatics)
					fmt.Println("↑工作文件夹已经确认")
					return
				}
			}
		}
		parent := filepath.Dir(wd)
		if parent == wd {
			break
		}
		wd = parent
	}
	panic("无法确定正确的工作路径")
}

// 开启本地测试服务器
func startWebServer(port int) {
	sv := &http.Server{Addr: fmt.Sprintf("localhost:%d", port)}
	handler := http.NewServeMux()
	handler.HandleFunc("/", handleHttpRequest)
	sv.Handler = handler
	fmt.Printf("网页服务器已准备：  http://%v  \n", sv.Addr)
	err := sv.ListenAndServe()
	if err != nil {
		panic(err)
	}
}

// 新建空白文章
func createNewPost(fileTitle string, isHtmlPost bool) {
	olds, err := loadPostFiles()
	if err != nil {
		fmt.Println("扫描已有的文章时出现错误")
		panic(err)
	}
	tags := make([]string, 0)
	for i := 0; i < len(olds); i++ {
		p := olds[i]
		if strings.EqualFold(p.Filetitle, fileTitle) {
			panic("文章文件名重复了 " + p.String())
		}
		for j := 0; j < len(p.Tags); j++ {
			t := p.Tags[j]
			if !IsStringInArray(tags, t, true) {
				tags = append(tags, t)
			}
		}
	}
	now := time.Now()
	p := new(Post)
	p.Filetitle = fileTitle
	p.Title = fileTitle
	p.Date = now
	dir := ""
	path := ""
	if isHtmlPost {
		dir = filepath.Join(fdPosts, now.Format("2006"), fileTitle)
		path = filepath.Join(dir, "index.html")
	} else {
		dir = filepath.Join(fdPosts, now.Format("2006"))
		path = filepath.Join(dir, fileTitle+".md")
	}
	fmt.Println("新文章文件路径：  ", path)
	err = os.MkdirAll(dir, os.ModePerm)
	if err != nil {
		panic(err)
	}
	writeFile := func(tmplText string, path string) {
		tmpl := template.New("")
		tmpl, err = tmpl.Parse(tmplText)
		if err != nil {
			panic(err)
		}
		stream, err := os.Create(path)
		if err != nil {
			panic(err)
		}
		defer stream.Close()
		err = tmpl.Execute(stream, p)
		if err != nil {
			panic(err)
		}
	}
	if isHtmlPost {
		writeFile(tmNewPostHtml, path)
		path = filepath.Join(dir, "tsconfig.json")
		writeFile(tmNewTsconfig, path)
	} else {
		writeFile(tmNewPostMd, path)
	}
	fmt.Println("生成完毕。")
	fmt.Println("历史可用标签：")
	sort.Strings(tags)
	fmt.Println(tags)
}

const (
	tmNewPostMd string = `---
title: {{.Title}}
date: {{.Date.Format "2006-01-02"}}
tags: [标签1,标签2]
---
从这一行开始写。
`
	tmNewPostHtml string = `<!--
title: {{.Title}}
date: {{.Date.Format "2006-01-02"}}
tags: [标签1,标签2]
-->
<p>从这一行开始写。</p>
`
	tmNewTsconfig string = `{
    "compilerOptions": {
        "target": "ES2021",
        "module": "amd",
        "outFile": "../../../statics/js/{{.Title}}.js",
        "rootDir": "./",
        "removeComments": true,
        "strict": true,
        "esModuleInterop": true,
        "skipLibCheck": true,
        "forceConsistentCasingInFileNames": true
    }
}`
)
