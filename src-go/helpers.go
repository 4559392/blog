package main

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"
	"unicode/utf8"
)

// 判断str是否在array里面
func IsStringInArray(array []string, str string, ignoreCase bool) bool {
	if len(array) < 1 {
		return false
	}
	if ignoreCase {
		for _, v := range array {
			if strings.EqualFold(str, v) {
				return true
			}
		}
	} else {
		for _, v := range array {
			if str == v {
				return true
			}
		}
	}
	return false
}

// 提取字符串中间的某一部分，从第 start 个字符之后的第一个字符开始截取，截取到第 ends 个字符过，ends 可以写0表示提取剩下的全部
func SubString(str string, start int, ends int) string {
	max := utf8.RuneCountInString(str)
	if max < 2 {
		return str
	}
	if start > max {
		return ""
	} else if start < 1 {
		start = 0
	}
	if ends < 1 || ends > max {
		ends = max - 1
	} else if ends < start {
		ends = start
	}
	return string([]rune(str)[start:ends])
}

// 布衣的文件缓存结构体
type ByFileCache struct {
	Path             string
	LastReadUnixTime int64
	Content          []byte
}

var allByFileCache = new(sync.Map)

// 对文件缓存进行自动刷新，如果文件的 modtime 更新了就重新读取，返回的 bool 表示是否重新读取了
func (f *ByFileCache) AutoRefresh() (bool, error) {
	info, err := os.Stat(f.Path)
	if err != nil {
		return false, err
	}
	if info.ModTime().Unix() >= f.LastReadUnixTime {
		ct, err := os.ReadFile(f.Path)
		if err == nil && ct != nil {
			f.LastReadUnixTime = time.Now().Unix()
			f.Content = ct
			return true, nil
		}
		return false, err
	}
	return false, nil
}

// 读取一个文件的全部内容，同时进行缓存，下次访问同一个文件就会直接给予上次的缓存
// 直到文件的修改时间变化了再重新读取
func ReadFileOrCache(path string) (data []byte, updated bool, err error) {
	path, err = filepath.Abs(path)
	if err != nil {
		return nil, false, err
	}
	value, hasCache := allByFileCache.Load(path)
	var cache *ByFileCache
	if hasCache {
		ok := false
		cache, ok = value.(*ByFileCache)
		if !ok {
			return nil, false, fmt.Errorf("无法转换数据到*fileCache %v", value)
		}
		updated, err = cache.AutoRefresh()
		if err != nil {
			return nil, false, err
		}
	} else {
		cache = new(ByFileCache)
		cache.Path = path
		updated, err = cache.AutoRefresh()
		if err == nil {
			allByFileCache.Store(path, cache)
		} else {
			return nil, false, err
		}
	}
	return cache.Content, updated, nil
}

// 把文件A复制到文件B，如果文件B已经存在，会直接覆盖，文件B必须是完整的路径，不能是文件夹的名字。
// 如果文件B的路径不存在，会自动创建对应的文件夹。返回的是复制的字节数。
func CopyFile(a string, b string) (int64, error) {
	var len int64 = -1
	sa, err := os.Open(a)
	if err != nil {
		return len, err
	}
	defer sa.Close()
	targetfolder := filepath.Dir(b)
	err = os.MkdirAll(targetfolder, os.ModePerm)
	if err != nil {
		return len, err
	}
	sb, err := os.Create(b)
	if err != nil {
		return len, err
	}
	defer sb.Close()
	len, err = io.CopyBuffer(sb, sa, make([]byte, 2048))
	return len, err
}

// 把文件夹root里的全部子文件（不包括空白子文件夹）复制到文件夹dest。
// 如果dest已经存在，会先删除整个dest。返回的是复制成功的文件个数。
func CopyFolder(root string, dest string, useLowerCaseFileName bool) (int, error) {
	count := 0
	err := os.RemoveAll(dest)
	if err != nil {
		return count, err
	}
	var fc = func(path string, d os.DirEntry, err error) error {
		if err != nil {
			return err
		}
		if d.IsDir() {
			return nil
		}
		rel, err := filepath.Rel(root, path)
		if err != nil {
			return err
		}
		if useLowerCaseFileName {
			rel = strings.ToLower(rel)
		}
		target := filepath.Join(dest, rel)
		_, err = CopyFile(path, target)
		if err != nil {
			return err
		}
		count += 1
		return nil
	}
	err = filepath.WalkDir(root, fc)
	if err != nil {
		return count, err
	}
	return count, nil
}
