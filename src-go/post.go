package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"path/filepath"
	"sort"
	"strings"
	"time"
	"unicode/utf8"

	"github.com/gomarkdown/markdown"
	"github.com/gomarkdown/markdown/parser"
)

// 文章结构体
type Post struct {
	Filetitle       string
	Title           string
	Date            time.Time
	Tags            []string
	IsHTMLPost      bool
	OriginalContent string
}

func (p *Post) String() string {
	return fmt.Sprintf("[(%v)%v(%v len:%d) ]", p.Filetitle, p.Title, p.Date.Format("2006-01-02"), len(p.OriginalContent))
}

type SortPostsByDate []*Post

func (a SortPostsByDate) Len() int      { return len(a) }
func (a SortPostsByDate) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a SortPostsByDate) Less(i, j int) bool {
	return a[i].Date.Unix() > a[j].Date.Unix()
}

// 根据路径解析文章的文件标题
func ParsePostFileTitleFromPath(path string) string {
	path = strings.ToLower(path)
	ft := ""
	if strings.HasSuffix(path, ".html") {
		ft = filepath.Base(filepath.Dir(path))
	} else {
		ft = strings.ReplaceAll(filepath.Base(path), filepath.Ext(path), "")
	}
	ft = strings.TrimSpace(ft)
	return ft
}

func (p *Post) HTMLContent() string {
	if p.IsHTMLPost {
		return p.OriginalContent
	}
	extensions := parser.Tables | parser.FencedCode | parser.LaxHTMLBlocks | parser.DefinitionLists | parser.HardLineBreak | parser.EmptyLinesBreakList
	parser := parser.NewWithExtensions(extensions)
	out := string(markdown.ToHTML([]byte(p.OriginalContent), parser, nil))
	return out
}

// 解析流并返回文章结构体
func ParsePostFromStream(fileTitle string, stream io.Reader) (*Post, error) {
	reader := bufio.NewReader(stream)
	var err error
	p := new(Post)
	line := ""
	parseHeaderValue := func(key string) (string, error) {
		if strings.HasPrefix(line, key+": ") {
			tt := strings.TrimSpace(SubString(line, utf8.RuneCountInString(key)+2, 0))
			if utf8.RuneCountInString(tt) > 0 {
				return tt, nil
			}
		}
		return "", fmt.Errorf("文章头部的 %v 不能为空 %v", key, fileTitle)
	}
	if utf8.RuneCountInString(fileTitle) < 1 {
		return nil, fmt.Errorf("文章文件名不能为空")
	}
	p.Filetitle = strings.ToLower(fileTitle)
	buffer := new(bytes.Buffer)
	lineIndex := 0
	for lineIndex = 1; lineIndex < 9999; lineIndex++ {
		line, err = reader.ReadString('\n')
		switch lineIndex {
		case 1:
			p.IsHTMLPost = !strings.HasPrefix(line, "---")
		case 2:
			line, err = parseHeaderValue("title")
			if err != nil {
				return nil, err
			}
			p.Title = line
		case 3:
			line, err = parseHeaderValue("date")
			if err != nil {
				return nil, err
			}
			tm, err := time.Parse("2006-01-02", line)
			if err != nil {
				return nil, fmt.Errorf("文章的时间格式无法解析 %v", fileTitle)
			}
			p.Date = tm
		case 4:
			line, err = parseHeaderValue("tags")
			if err != nil {
				return nil, err
			}
			line = strings.Trim(line, "[],")
			tags := strings.Split(line, ",")
			for j := 0; j < len(tags); j++ {
				tag := strings.TrimSpace(tags[j])
				if utf8.RuneCountInString(tag) < 1 {
					continue
				}
				tag = strings.ToLower(tag)
				if IsStringInArray(p.Tags, tag, true) {
					continue
				}
				p.Tags = append(p.Tags, tag)
			}
			if len(p.Tags) < 1 {
				return nil, fmt.Errorf("文章不能没有tag %v", fileTitle)
			}
			sort.Strings(p.Tags)
		case 5:
			continue
		default:
			buffer.WriteString(line)
		}
		if err == io.EOF {
			break
		}
	}
	if lineIndex < 6 {
		return nil, fmt.Errorf("文章头部信息不完整 %v", fileTitle)
	}
	p.OriginalContent = strings.ReplaceAll(strings.ReplaceAll(strings.TrimSpace(buffer.String()), "\r\n", "\n"), "\r", "\n")
	if len(p.OriginalContent) < 1 {
		return nil, fmt.Errorf("文章正文不能为空白 %v", fileTitle)
	}
	return p, nil
}
