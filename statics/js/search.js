"use strict";
const searchInput = document.getElementById("inputsearch");
const div = document.getElementById("searchOptions");
const links = [];
const sources = new Map();
function addSearch(title, url) {
    sources.set(title, url);
    let button = document.createElement("button");
    button.style.margin = "4px";
    button.innerText = title;
    button.addEventListener('click', onButtonPressed);
    div.appendChild(button);
    links.push(button);
}
function onButtonPressed(e) {
    let str = searchInput.value.trim();
    let url = sources.get(this.innerText);
    if (url == null) {
        return;
    }
    url = url.replace("输入", str);
    location.href = url;
}
addSearch("DuckDuckGo", "https://duckduckgo.com/?q=site%3Abuyi.dev+输入");
addSearch("谷歌", "https://www.google.com/search?newwindow=1&safe=strict&q=site%3Abuyi.dev+输入");
