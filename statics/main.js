"use strict";
const None = "none";
const Block = "block";
const mainBody = document.getElementsByTagName("main")[0];
(function () {
    const divSort = document.createElement("div");
    divSort.style.marginTop = "10px";
    const labSort = document.createElement("span");
    labSort.innerText = "";
    divSort.appendChild(labSort);
    const butViewAll = document.createElement("a");
    butViewAll.innerText = "取消筛选";
    butViewAll.href = "/#";
    butViewAll.style.marginLeft = '20px';
    divSort.appendChild(butViewAll);
    mainBody.insertBefore(divSort, mainBody.firstChild);
    const sortTags = function () {
        const t = decodeURIComponent(location.hash.toLowerCase().replaceAll("#", ""));
        const viewAll = t.length < 1;
        divSort.style.display = viewAll ? None : Block;
        labSort.innerText = "筛选标签：  " + t;
        window.scroll(0, 0);
        const posts = mainBody.getElementsByClassName("articleBox");
        const tMark = "," + t + ",";
        for (let i = 0; i < posts.length; i++) {
            const div = posts[i];
            if (viewAll) {
                div.style.display = Block;
            }
            else {
                const tags = div.getAttribute("data-tags");
                if (tags == null) {
                    continue;
                }
                div.style.display = tags.includes(tMark) ? Block : None;
            }
        }
    };
    window.addEventListener("hashchange", sortTags);
    sortTags();
})();
(function () {
    const articleFull = document.getElementsByClassName("articleFull")[0];
    if (articleFull == null) {
        return;
    }
    const divcontent = document.getElementsByClassName("articleContent")[0];
    const codelist = divcontent.querySelectorAll("code");
    if (codelist.length > 0) {
        const js = document.createElement("script");
        js.src = "/highlight.min.js";
        js.addEventListener("load", function () {
            hljs.highlightAll();
        });
        document.body.appendChild(js);
        const ShowCode = "显示代码";
        const HideCode = "隐藏代码";
        const CopyCode = "复制代码";
        const cantCopy = navigator.clipboard == null;
        const AfterCopyCode = "复制成功！";
        const CantCopyCode = "代码复制失败";
        for (const codeElement of codelist) {
            const parent = codeElement.parentElement;
            if (parent == null) {
                throw " <code> 的父元素是 null";
            }
            if (parent.tagName.toUpperCase() == 'PRE') {
                const pre = parent;
                pre.className += ' hljs';
                const txt = pre.innerText;
                const divBefore = document.createElement("div");
                divBefore.className = "codeBefore";
                const butCopy = document.createElement("button");
                butCopy.innerText = CopyCode;
                let lastTimer = 0;
                butCopy.addEventListener("click", function () {
                    if (cantCopy) {
                        butCopy.innerText = CantCopyCode;
                        return;
                    }
                    navigator.clipboard.writeText(txt);
                    butCopy.innerText = AfterCopyCode;
                    if (lastTimer != 0) {
                        clearTimeout(lastTimer);
                    }
                    lastTimer = setTimeout(function () {
                        butCopy.innerText = CopyCode;
                        lastTimer = 0;
                    }, 1300);
                });
                divBefore.appendChild(butCopy);
                if (txt.length > 500 || txt.split("\n").length > 9) {
                    const divPreview = document.createElement("code");
                    divPreview.className = 'hljs';
                    divPreview.innerText = txt.substring(0, 15).trim().replaceAll(/[\n\r]+/gim, " ") + " ......";
                    const butHide = document.createElement("button");
                    butHide.innerText = ShowCode;
                    butHide.addEventListener("click", function () {
                        if (pre.style.display == None) {
                            pre.style.display = Block;
                            butHide.innerText = HideCode;
                            divPreview.style.display = None;
                        }
                        else {
                            pre.style.display = None;
                            butHide.innerText = ShowCode;
                            divPreview.style.display = Block;
                        }
                    });
                    butHide.dispatchEvent(new Event("click"));
                    divBefore.appendChild(butHide);
                    divBefore.appendChild(divPreview);
                }
                const outbox = pre.parentElement;
                outbox.insertBefore(divBefore, pre);
            }
            else {
                codeElement.className += ' hljs';
            }
        }
    }
})();
(function () {
    const imgs = document.getElementsByTagName("img");
    for (let i = 0; i < imgs.length; i++) {
        const img = imgs[i];
        let s = img.src;
        if (s.length > 25) {
            s = s.substring(0, 25) + "...";
        }
        img.alt = "图片加载失败 " + s;
    }
})();
(function () {
    const links = document.getElementsByTagName("a");
    const myHost = location.host;
    for (let i = 0; i < links.length; i++) {
        const a = links[i];
        let jumpopen = false;
        if (a.host.length > 0 && a.host != myHost) {
            jumpopen = true;
        }
        else {
            let p = a.pathname.toLowerCase();
            if (p.endsWith(".js") || p.endsWith(".xml")) {
                jumpopen = true;
            }
        }
        if (jumpopen) {
            a.target = "_blank";
        }
    }
})();
(function () {
    const es = document.getElementsByTagName("time");
    for (let i = 0; i < es.length; i++) {
        const tm = es[i];
        const dt = new Date(tm.dateTime);
        if (dt.getFullYear() < 2017) {
            tm.style.display = None;
        }
    }
})();
