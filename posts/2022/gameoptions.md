---
title: 我的游戏启动参数
date: 2022-01-31
tags: [steam,笔记]
---
# Grand Theft Auto V  
[官方参考](https://support.rockstargames.com/articles/202518358/Available-Command-Lines-for-GTAV-on-PC)   
低画质：  
`-windowed -width 1600 -height 900 -frameLimit 1 -ignoreDifferentVideoCard -DX10`   
高画质：  
`-windowed -width 1600 -height 900 -ignoreDifferentVideoCard -DX11 -anisotropicQualityLevel 16 -frameLimit 1 -particleShadows 1 -shadowQuality 3 -textureQuality 2`   
离线： 
`-scofflineonly`   

# Grand Theft Auto IV
`-windowed -width 1600 -height 900`

# Max Payne 3
[官方参考](https://support.rockstargames.com/zh/articles/200152476/Full-list-of-command-line-parameters-for-Max-Payne-3-on-PC)    
`-windowed -width 1600 -height 900 -dx9`

# Left 4 Dead 2
[官方参考](https://developer.valvesoftware.com/wiki/Command_Line_Options)   
`-windowed -w 1600 -h 900 -console -novid +sv_consistency 0`   

# Counter-Strike: Global Offensive
`-windowed -w 1600 -h 900 -console -worldwide`   

# Counter-Strike (GoldSource)
`-windowed -w 1600 -h 900 -console +hud_fastswitch 1 +mp_consistency 0`

# Garry's Mod 按键：
```
bind end +smh_playback  
bind m +smh_menu
BindToggle l vcollide_wireframe 0 1
```
