---
title: 我被 EA 封号
date: 2022-09-23
tags: [杂]
---

# 九月16日 09:36  
我注册了一个新的 EA Origin 账户。   

> 欢迎您访问自己的EA账户！
> 您的EA账户是您访问EA任何内容的通用通行证，包括网站、移动应用、游戏机和PC游戏。

# 九月18日 08:17  
我用大陆支付宝（借记卡）付款，购买了 [《戰地風雲4》高級豪華版](https://www.origin.com/hkg/en-us/store/battlefield/battlefield-4) 。  
现在是打折期间，花了50.27 元。   

![](/images/ea1.webp)   

# 九月19日 06:48
EA 给我发了一封红色邮件。   
我一开始没有注意到邮件，我只是试着启动游戏，然后会闪退，弹出警告说“无法连接到EA服务器”。   

![](/images/ea2.webp)   
![](/images/ea3.webp)   

在客服网站的 Ban History 里面，可以看见封号，但是 Reason 是空白的。真就无理由乱封。   

![](/images/ea4.webp)   

我在客服网站里发送了简短的英文申请案件，进行申诉。附带一张支付宝内的支付明细截图。   

# 九月20日 12:51

> Hello,
> Thanks for waiting while we investigate your query about Origin.
> After we review your case, we will update you as soon as possible.

他们经过了初步检查，觉得我的申请不是恶意的，开始正式调查。   

# 九月22日 13:15

> Hello,
> We recently notified you about a sanction placed on your EA account. After further review, this sanction has been removed.

EA 解封了我的账户。我的申请案件变成了 `overturn` 。  

# 九月23日 07:05
我申请退款了。   
反正我游戏连一分钟都没真正玩过。   
不知道 EA 能不能、要多久才能把钱还回来。   

> Thank you for ordering from the Origin Store. We’re sorry to hear this game didn’t work out for you, and we’re happy to help. 

# 十月01日 11:50
我检查了一下邮箱和 EA Help 官方网站，依然是没有任何结果。   
[EA 网站](https://www.origin.com/hkg/zh-tw/store/great-game-guarantee-terms)说的是会在几天内回复。这都 tm 多少天了。   

> 若要申請退款，請前往「[訂購記錄](https://myaccount.ea.com/cp-ui/orderhistory/index?locale=zh_TW&env=prod&sourceType=web&gameId=ebisu)」頁面，然後選取符合資格之商品旁的「申請退款」連結。填寫並提交[退款申請表](https://help.ea.com/tw/refund/)。您會在提交申請的四十八 (48) 小時內獲得回應，若您符合優質遊戲保證的退款條件，則會在七 (7) 至十 (10) 天內獲得退款。

