---
title: GMod地图： Win10 Logo
date: 2022-02-07
tags: [source,Steam]
---
春节假期的时候闲来无事，模仿了[真正的 Windows 10 Logo 的拍摄流程](https://www.youtube.com/watch?v=ewmXizBqjl0)，做了一张 Garry's Mod Sandbox 的地图。   

幸运的是地图在 GMod Workshop 拿了五星（160 个赞，360 个收藏，3100个订阅），还上首页了。（具体是北京时间2022年2月7日上午10点）   
订阅链接： [点我](https://steamcommunity.com/sharedfiles/filedetails/?id=2740983801)   
源文件 VMF 下载： [Google Drive](https://drive.google.com/file/d/11gxEQe-1YjdI7APIsxRGp6XfyqZfQ6HT/view?usp=sharing)
![](https://s4.ax1x.com/2022/02/07/HMe0BD.png)   

真正刺激我做这张地图的关键，其实还是在玩[超阈值空间](https://store.steampowered.com/app/1049410)的时候，碰见的神似 Win8Logo 的那个场面。  
![](https://s4.ax1x.com/2022/02/07/HMehDS.png)   

这个地图的核心技术关键在于使用 [func_dustcloud](https://developer.valvesoftware.com/wiki/Func_dustcloud) 来模拟激光穿过尘埃的效果，如果不添加这个，那起源引擎地图的空气质量都好的一批。    
如果用的是 fog 效果，就失去了动态的效果，感觉死气沉沉。   
其次是把接受投影的墙壁的贴图的 [lightmap](https://developer.valvesoftware.com/wiki/Lightmap) 尺寸都改成很小，这样才能清晰地看见探照灯穿过窗户格子细线的投影。   
![](https://s4.ax1x.com/2022/02/07/HMn2m8.png)   
我也故意把窗户搭建在空中，并且把两边的墙壁延伸地很长，这样可以避免拍照截图的时候拍到墙角显得很不自然。   
![](https://s4.ax1x.com/2022/02/07/HMn5fs.png)   
