/// <reference path="init.ts" />

if (location.host.includes('weibo.c')) {
    // 打开链接到新标签页，并复制到剪贴板
    function OpenURL(u: string): void {
        GM_setClipboard(u)
        GM_openInTab(u, { insert: true, active: true })
    }

    if (location.host.endsWith('weibo.com')) {
        GM_registerMenuCommand("提取H5分享链接", function () {
            const url = location.pathname
            // 判断是不是单条微博
            let reg = /^\/[0-9]+\/([a-z|A-Z|0-9]+)$/
            if (url.includes("/profile") == false) {
                const matches = reg.exec(url)
                if (matches != null) {
                    const bid = matches[1]
                    OpenURL("https://m.weibo.cn/status/" + bid)
                    return
                }
            }
            // 判断是不是头条文章
            if (url.includes("ttarticle/p")) {
                const s = new URLSearchParams(location.search)
                const id = s.get("id")
                if (id != null && id.length > 0) {
                    OpenURL("https://card.weibo.com/article/m/show/id/" + id)
                    return
                }
            }
            // 从 $config 里面查找当前浏览的用户的数字 id ，仅适用于旧版pc微博
            const cfg = $CONFIG
            if (cfg != null) {
                if (cfg.oid != null) {
                    const oid = parseInt(cfg.oid)
                    if (isFinite(oid) && oid > 0) {
                        OpenURL("https://m.weibo.cn/u/" + oid.toString())
                        return
                    }
                }
            }
            // 从 <main> 里提取当前浏览的用户的数字 id ，仅适用于新版pc微博，此处兼容了 https://weibo.com/yigeking/ 这样的情况
            const mainDiv = document.getElementsByTagName("main")[0] as HTMLDivElement | null
            if (mainDiv != null) {
                const elements = mainDiv.getElementsByTagName("a")
                const searchURL = "/u/page/follow/"
                const searchURLlen = searchURL.length
                for (let index = 0; index < elements.length; index++) {
                    const ac = elements[index] as HTMLAnchorElement
                    const p = ac.pathname
                    if (p.length > searchURLlen && p.startsWith(searchURL)) {
                        const uid = parseInt(p.substring(searchURLlen))
                        if (isFinite(uid) && uid > 0) {
                            OpenURL("https://m.weibo.cn/u/" + uid.toString())
                            return
                        }
                    }
                }
            }
            alert("无法读取微博的相关信息。")
        })
    } else {
        GM_registerMenuCommand("跳转到PC版微博", function () {
            const url = location.pathname
            // 判断是不是单条微博
            let reg = /^\/(status|detail)\/([a-z|A-Z|0-9]+)$/
            let matches = reg.exec(url)
            if (matches != null) {
                const cfg = $render_data
                if (cfg != null) {
                    const data = cfg.status
                    if (data != null) {
                        const bid = data.bid
                        const user = data.user
                        if (bid != null && user != null) {
                            const uid = user.id
                            if (uid != null) {
                                OpenURL("https://weibo.com/" + uid + "/" + bid)
                                return
                            }
                        }
                    }
                }
            }
            // 判断是不是头条文章
            reg = /article\/m\/show\/id\/([0-9]+)$/
            matches = reg.exec(url)
            if (matches != null) {
                const id = matches[1]
                OpenURL("https://weibo.com/ttarticle/p/show?id=" + id)
                return
            }
            // 判断是不是用户的个人主页
            reg = /^\/(profile|u)\/([0-9]+)/
            matches = reg.exec(url)
            if (matches != null) {
                const uid = matches[2]
                OpenURL("https://weibo.com/u/" + uid)
                return
            }
            alert("无法读取微博的相关信息。")
        })
    }
    
    GM_registerMenuCommand("如跳转异常，可先刷新本页后重试", function () {
        location.reload()
    })
}