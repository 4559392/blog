interface weiboConfig {
    oid: string | null
}

const $CONFIG: weiboConfig | null

interface weiboRenderData {
    status: {
        bid: string | null,
        user: { id: string | null } | null
    } | null
}

const $render_data: weiboRenderData | null