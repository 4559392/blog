// ==UserScript==
// @name        陈布衣的垃圾脚本集
// @namespace   https://buyi.dev/
// @version     2022.08.24.1
// @description https://buyi.dev/trashscripts
// @author      Chen Buyi
// @match       https://twitter.com/*
// @match       https://card.weibo.com/article/*
// @match       https://weibo.com/*
// @match       https://www.weibo.com/*
// @match       https://m.weibo.cn/status*
// @match       https://m.weibo.cn/detail*
// @match       https://m.weibo.cn/profile*
// @match       https://m.weibo.cn/u/*
// @match       https://steamcommunity.com/*/friends*
// @match       https://mail.qq.com/cgi-bin/laddr_list?*
// @grant       GM_registerMenuCommand
// @grant       GM_setClipboard
// @grant       GM_openInTab
// ==/UserScript==

interface KeyValue {
    Key: string
    Value: string
}
