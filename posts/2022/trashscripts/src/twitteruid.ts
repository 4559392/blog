/// <reference path="init.ts" />

if (location.host == "twitter.com") {
    GM_registerMenuCommand("提取userID", function () {
        if (location.pathname.includes("/status/")) {
            alert("出错： 请在需要提取ID的用户的个人主页进行操作")
            return
        }
        let elements = document.getElementsByTagName("main")
        if (elements.length < 1) {
            alert("出错： <main> 元素不存在")
            return
        }
        let main = elements[0] as HTMLDivElement
        elements = main.getElementsByTagName("a")
        const reg = /\/i\/connect_people\?user_id=([0-9]+)/
        for (let i = 0; i < elements.length; i++) {
            const link = elements[i] as HTMLAnchorElement
            let url = link.href
            const match = reg.exec(url)
            if (match != null) {
                const uid = match[1]
                url = "https://twitter.com/i/user/" + uid
                GM_setClipboard(url)
                alert("TA的推特数字id是：" + uid + "\n我已经把数字id主页链接复制到剪贴板里了。")
                return
            }
        }
        alert("出错： 我找不到此人的推特数字 id")
        return
    })
}
