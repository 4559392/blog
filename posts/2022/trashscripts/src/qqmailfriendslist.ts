/// <reference path="init.ts" />

if (location.host.endsWith('mail.qq.com')) {
    const numQQaddress = /[0-9]{5,11}@qq.com/i
    function TakeQQFriends() {
        const items = document.querySelectorAll('.list_item')
        if (items.length < 1) {
            alert("此页面一个好友div都没有")
            return
        }
        let array: Array<KeyValue> = []
        for (const element of items) {
            let spans = element.getElementsByClassName('email tf')
            if (spans.length < 1) {
                alert('无法获取 class:email tf')
                return
            }
            let span = spans[0] as HTMLSpanElement
            let kv: KeyValue = { Key: "", Value: "" }
            let tmp = span.innerText.normalize()
            if (numQQaddress.test(tmp)) {
                tmp = tmp.replaceAll('@qq.com', '')
            }
            kv.Key = tmp
            spans = element.getElementsByClassName('name tf')
            if (spans.length < 1) {
                alert('无法获取 class:name tf')
                return
            }
            span = spans[0] as HTMLSpanElement
            tmp = span.innerText.trim().normalize()
            kv.Value = tmp
            array.push(kv)
        }
        array = array.sort(function (a, b) {
            return a.Key.localeCompare(b.Key)
        })
        let out = `总数： ${array.length}\n导出时间： ${(new Date()).toLocaleString()}\n`
        for (const kv of array) {
            out += `\n${kv.Key}    ${kv.Value}`
        }
        GM_setClipboard(out)
        alert(`已经把数据复制到了剪贴板里，一共有 ${array.length} 个QQ好友。`)
    }
    const butTake = document.createElement("button")
    butTake.innerText = "复制好友列表"
    butTake.addEventListener("click", TakeQQFriends)
    butTake.style.position = "fixed"
    butTake.style.right = "35px"
    butTake.style.top = "5px"
    butTake.style.zIndex = "999999"
    document.body.appendChild(butTake)
    console.log(butTake)
}