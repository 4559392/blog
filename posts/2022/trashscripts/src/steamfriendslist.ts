/// <reference path="init.ts" />

if (location.host.endsWith('steamcommunity.com')) {
    GM_registerMenuCommand("提取Steam好友列表", function () {
        const divs = document.getElementsByClassName("friend_block_v2")
        if (divs.length < 1) {
            alert("此页面一个朋友div都没有")
            return
        }
        let array: Array<KeyValue> = []
        for (let i = 0; i < divs.length; i++) {
            const div = divs[i] as HTMLDivElement
            const spans = div.getElementsByTagName("span")
            for (let j = 0; j < spans.length; j++) {
                const span = spans[j]
                span.style.display = 'none'
            }
            const kv: KeyValue = {
                Key: "",
                Value: ""
            }
            let tmp = div.getAttribute("data-steamid")
            if (tmp == null || tmp.length != 17) {
                alert("出错：无法获取 data-steamid")
                return
            }
            kv.Key = tmp
            tmp = div.innerText.replace(/[\r|\n]/gim, "")
            kv.Value = tmp.normalize()
            array.push(kv)
            for (let j = 0; j < spans.length; j++) {
                const span = spans[j]
                span.style.display = "inline"
            }
        }
        array = array.sort(function (a, b) {
            return a.Key.localeCompare(b.Key)
        })
        let out = `总数： ${array.length}\n导出时间： ${(new Date()).toLocaleString()}\n`
        for (const kv of array) {
            out += `\n${kv.Key}    ${kv.Value}`
        }
        GM_setClipboard(out)
        alert(`已经把数据复制到了剪贴板里，一共有 ${array.length} 个Steam好友。`)
    })
}