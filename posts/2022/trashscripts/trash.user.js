"use strict";
// ==UserScript==
// @name        陈布衣的垃圾脚本集
// @namespace   https://buyi.dev/
// @version     2022.08.24.1
// @description https://buyi.dev/trashscripts
// @author      Chen Buyi
// @match       https://twitter.com/*
// @match       https://card.weibo.com/article/*
// @match       https://weibo.com/*
// @match       https://www.weibo.com/*
// @match       https://m.weibo.cn/status*
// @match       https://m.weibo.cn/detail*
// @match       https://m.weibo.cn/profile*
// @match       https://m.weibo.cn/u/*
// @match       https://steamcommunity.com/*/friends*
// @match       https://mail.qq.com/cgi-bin/laddr_list?*
// @grant       GM_registerMenuCommand
// @grant       GM_setClipboard
// @grant       GM_openInTab
// ==/UserScript==
/// <reference path="init.ts" />
if (location.host.endsWith('mail.qq.com')) {
    const numQQaddress = /[0-9]{5,11}@qq.com/i;
    function TakeQQFriends() {
        const items = document.querySelectorAll('.list_item');
        if (items.length < 1) {
            alert("此页面一个好友div都没有");
            return;
        }
        let array = [];
        for (const element of items) {
            let spans = element.getElementsByClassName('email tf');
            if (spans.length < 1) {
                alert('无法获取 class:email tf');
                return;
            }
            let span = spans[0];
            let kv = { Key: "", Value: "" };
            let tmp = span.innerText.normalize();
            if (numQQaddress.test(tmp)) {
                tmp = tmp.replaceAll('@qq.com', '');
            }
            kv.Key = tmp;
            spans = element.getElementsByClassName('name tf');
            if (spans.length < 1) {
                alert('无法获取 class:name tf');
                return;
            }
            span = spans[0];
            tmp = span.innerText.trim().normalize();
            kv.Value = tmp;
            array.push(kv);
        }
        array = array.sort(function (a, b) {
            return a.Key.localeCompare(b.Key);
        });
        let out = `总数： ${array.length}\n导出时间： ${(new Date()).toLocaleString()}\n`;
        for (const kv of array) {
            out += `\n${kv.Key}    ${kv.Value}`;
        }
        GM_setClipboard(out);
        alert(`已经把数据复制到了剪贴板里，一共有 ${array.length} 个QQ好友。`);
    }
    const butTake = document.createElement("button");
    butTake.innerText = "复制好友列表";
    butTake.addEventListener("click", TakeQQFriends);
    butTake.style.position = "fixed";
    butTake.style.right = "35px";
    butTake.style.top = "5px";
    butTake.style.zIndex = "999999";
    document.body.appendChild(butTake);
    console.log(butTake);
}
/// <reference path="init.ts" />
if (location.host.endsWith('steamcommunity.com')) {
    GM_registerMenuCommand("提取Steam好友列表", function () {
        const divs = document.getElementsByClassName("friend_block_v2");
        if (divs.length < 1) {
            alert("此页面一个朋友div都没有");
            return;
        }
        let array = [];
        for (let i = 0; i < divs.length; i++) {
            const div = divs[i];
            const spans = div.getElementsByTagName("span");
            for (let j = 0; j < spans.length; j++) {
                const span = spans[j];
                span.style.display = 'none';
            }
            const kv = {
                Key: "",
                Value: ""
            };
            let tmp = div.getAttribute("data-steamid");
            if (tmp == null || tmp.length != 17) {
                alert("出错：无法获取 data-steamid");
                return;
            }
            kv.Key = tmp;
            tmp = div.innerText.replace(/[\r|\n]/gim, "");
            kv.Value = tmp.normalize();
            array.push(kv);
            for (let j = 0; j < spans.length; j++) {
                const span = spans[j];
                span.style.display = "inline";
            }
        }
        array = array.sort(function (a, b) {
            return a.Key.localeCompare(b.Key);
        });
        let out = `总数： ${array.length}\n导出时间： ${(new Date()).toLocaleString()}\n`;
        for (const kv of array) {
            out += `\n${kv.Key}    ${kv.Value}`;
        }
        GM_setClipboard(out);
        alert(`已经把数据复制到了剪贴板里，一共有 ${array.length} 个Steam好友。`);
    });
}
/// <reference path="init.ts" />
if (location.host == "twitter.com") {
    GM_registerMenuCommand("提取userID", function () {
        if (location.pathname.includes("/status/")) {
            alert("出错： 请在需要提取ID的用户的个人主页进行操作");
            return;
        }
        let elements = document.getElementsByTagName("main");
        if (elements.length < 1) {
            alert("出错： <main> 元素不存在");
            return;
        }
        let main = elements[0];
        elements = main.getElementsByTagName("a");
        const reg = /\/i\/connect_people\?user_id=([0-9]+)/;
        for (let i = 0; i < elements.length; i++) {
            const link = elements[i];
            let url = link.href;
            const match = reg.exec(url);
            if (match != null) {
                const uid = match[1];
                url = "https://twitter.com/i/user/" + uid;
                GM_setClipboard(url);
                alert("TA的推特数字id是：" + uid + "\n我已经把数字id主页链接复制到剪贴板里了。");
                return;
            }
        }
        alert("出错： 我找不到此人的推特数字 id");
        return;
    });
}
/// <reference path="init.ts" />
if (location.host.includes('weibo.c')) {
    // 打开链接到新标签页，并复制到剪贴板
    function OpenURL(u) {
        GM_setClipboard(u);
        GM_openInTab(u, { insert: true, active: true });
    }
    if (location.host.endsWith('weibo.com')) {
        GM_registerMenuCommand("提取H5分享链接", function () {
            const url = location.pathname;
            // 判断是不是单条微博
            let reg = /^\/[0-9]+\/([a-z|A-Z|0-9]+)$/;
            if (url.includes("/profile") == false) {
                const matches = reg.exec(url);
                if (matches != null) {
                    const bid = matches[1];
                    OpenURL("https://m.weibo.cn/status/" + bid);
                    return;
                }
            }
            // 判断是不是头条文章
            if (url.includes("ttarticle/p")) {
                const s = new URLSearchParams(location.search);
                const id = s.get("id");
                if (id != null && id.length > 0) {
                    OpenURL("https://card.weibo.com/article/m/show/id/" + id);
                    return;
                }
            }
            // 从 $config 里面查找当前浏览的用户的数字 id ，仅适用于旧版pc微博
            const cfg = $CONFIG;
            if (cfg != null) {
                if (cfg.oid != null) {
                    const oid = parseInt(cfg.oid);
                    if (isFinite(oid) && oid > 0) {
                        OpenURL("https://m.weibo.cn/u/" + oid.toString());
                        return;
                    }
                }
            }
            // 从 <main> 里提取当前浏览的用户的数字 id ，仅适用于新版pc微博，此处兼容了 https://weibo.com/yigeking/ 这样的情况
            const mainDiv = document.getElementsByTagName("main")[0];
            if (mainDiv != null) {
                const elements = mainDiv.getElementsByTagName("a");
                const searchURL = "/u/page/follow/";
                const searchURLlen = searchURL.length;
                for (let index = 0; index < elements.length; index++) {
                    const ac = elements[index];
                    const p = ac.pathname;
                    if (p.length > searchURLlen && p.startsWith(searchURL)) {
                        const uid = parseInt(p.substring(searchURLlen));
                        if (isFinite(uid) && uid > 0) {
                            OpenURL("https://m.weibo.cn/u/" + uid.toString());
                            return;
                        }
                    }
                }
            }
            alert("无法读取微博的相关信息。");
        });
    }
    else {
        GM_registerMenuCommand("跳转到PC版微博", function () {
            const url = location.pathname;
            // 判断是不是单条微博
            let reg = /^\/(status|detail)\/([a-z|A-Z|0-9]+)$/;
            let matches = reg.exec(url);
            if (matches != null) {
                const cfg = $render_data;
                if (cfg != null) {
                    const data = cfg.status;
                    if (data != null) {
                        const bid = data.bid;
                        const user = data.user;
                        if (bid != null && user != null) {
                            const uid = user.id;
                            if (uid != null) {
                                OpenURL("https://weibo.com/" + uid + "/" + bid);
                                return;
                            }
                        }
                    }
                }
            }
            // 判断是不是头条文章
            reg = /article\/m\/show\/id\/([0-9]+)$/;
            matches = reg.exec(url);
            if (matches != null) {
                const id = matches[1];
                OpenURL("https://weibo.com/ttarticle/p/show?id=" + id);
                return;
            }
            // 判断是不是用户的个人主页
            reg = /^\/(profile|u)\/([0-9]+)/;
            matches = reg.exec(url);
            if (matches != null) {
                const uid = matches[2];
                OpenURL("https://weibo.com/u/" + uid);
                return;
            }
            alert("无法读取微博的相关信息。");
        });
    }
    GM_registerMenuCommand("如跳转异常，可先刷新本页后重试", function () {
        location.reload();
    });
}
