---
title: GMod & L4D2 地图：神秘营地
date: 2022-05-02
tags: [source,steam]
---
这篇文章是讲述之前我已经发布过的仿制穿越火线CF挑战模式地图——“神秘营地”的。   
这地图是我一点一点对着CF自己游玩的截图和录像弄的，贴图也是自己抠的。   

- [GMod Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=2783287139)
- [L4D2 Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=2783025021)
- [B站视频](https://www.bilibili.com/video/BV1s94y1f7LA)
- [源文件](https://drive.google.com/file/d/1SY-_4JH66js8wf94wldj6VaX9r9Cue1m/view?usp=sharing)

这地图在 workshop 里面没有什么人来看，但是在B站收割了4万播放量，换了一百多块钱。   
因为B站提现的底线是100元，所以我上次提现之后多出来的3万播放量换的一些小钱就没能提现出来。  

## 截图
![](https://s1.ax1x.com/2022/05/02/OP3n6P.png)
![](https://s1.ax1x.com/2022/05/02/OP33kQ.png)

## 绝命之谷
其实在那之后我继续创作了“神秘营地”的孪生兄弟——“绝命之谷”，奈何那几天精神爆炸，我就没有继续创作了。重装了系统并且故意不备份工程和资源文件，一切都被我格式化掉了。   
留下了一张截图：   

![](https://s1.ax1x.com/2022/05/02/OPG9aD.png)  

还有一张游戏内的截图：    
![](https://s1.ax1x.com/2022/05/02/OPJuOx.png)   
