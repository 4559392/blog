---
title: 订阅 RSS
date: 2010-01-13
tags: 我自己
---

RSS 订阅链接：  
```
https://buyi.dev/atom.xml
```
[点我直接打开](/atom.xml)

该 RSS 已经通过 W3C 验证检测。  
[![](https://validator.w3.org/feed/images/valid-atom.png)](https://validator.w3.org/feed/check.cgi?url=https://buyi.dev/atom.xml)   

我一直在用浏览器扩展 [FeedBro](https://nodetics.com/feedbro/) ，支持大部分现代浏览器，而且还受到了 Mozilla 的官方推荐。    
