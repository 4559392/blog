---
title: 关于陈布衣
date: 2010-01-01
tags: 我自己
---
姓陈，号布衣   
Chen Buyi 

- 高考到本科线了  
- 喜欢写 Go, Typescript, Visual Basic .NET   
- 想变成可爱的女生，想有个家  
- 拉黑父母一年半载了  

曾用名：戈登走過去 Gordon Walkedby    
你可能会听说我之前是做B站视频或者 GMOD 游戏插件的，但是我现在都不做了。   

我一开始写的是“字布衣”，后面我想起来一个事情：表字最好是请别的文人给取一个，用做他人称呼用。   
自己取也行，但是自己称呼自己的字，是不礼貌的。   
我就现在改成号布衣了。假如某天我遇上一个文人瞧得起我，再让他赐我一个表字好了。   

# 联系布衣
- [Google 匿名留言，无需注册登录](https://docs.google.com/forms/d/e/1FAIpQLSfMsyyOhAAKQkMe8Lx9FIxaMBKsjnBWv89Mlmj0utfWwPKFWg/viewform)
- [Steam 主页留言](https://steamcommunity.com/profiles/76561198099466387)

头像是[捏的](https://www.neka.cc/composer/11109)，原作者是[梦境潜逃](https://www.neka.cc/users/7fff1a6c-95ef-5e35-9440-6073be17029f)。    
头像上的帽子是 [ながユー](https://www.pixiv.net/users/2509595) 画的帽子，来源是 [这里](https://www.pixiv.net/artworks/100136546) 。    

![](/images/avatar.webp)

![](/images/nz1.webp)

![](/images/nz2.webp)