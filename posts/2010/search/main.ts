const searchInput = document.getElementById("inputsearch") as HTMLInputElement
const div = document.getElementById("searchOptions") as HTMLDivElement

const links: Array<HTMLButtonElement> = []
const sources = new Map<string, string>()

function addSearch(title: string, url: string) {
    sources.set(title, url)
    let button = document.createElement("button")
    button.style.margin = "4px"
    button.innerText = title
    button.addEventListener('click', onButtonPressed)
    div.appendChild(button)
    links.push(button)
}

function onButtonPressed(this: HTMLButtonElement, e: MouseEvent) {
    let str = searchInput.value.trim()
    let url = sources.get(this.innerText)
    if (url == null) { return }
    url = url.replace("输入", str)
    location.href = url
}

addSearch("DuckDuckGo", "https://duckduckgo.com/?q=site%3Abuyi.dev+输入")
addSearch("谷歌", "https://www.google.com/search?newwindow=1&safe=strict&q=site%3Abuyi.dev+输入")
