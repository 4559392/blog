---
title: 在 Windows 上给 Git SSH 设置代理
date: 2019-09-01
tags: [笔记]
---
百度搜到的很多给 git 设置代理的办法，都是给 http 设置代理，而不是给 SSH 设置代理。   
那个 `git config --global http.proxy http://127.0.0.1:1082` 设置起来，只针对 http 的 git 有效果。     
然后就是 `ProxyCommand nc -v -x 127.0.0.1:1082 %h %p` ，那个分明是给 linux 才能用的， windows 上哪里来的 nc 程序？  

# 我的操作：  
在自己的用户文件夹找到 .ssh 文件夹，比如我的是 `C:\Users\wby\.ssh` ，在里面新建一个空白文件，取名 `config`。  
注意不是 `config.txt` ！  
我强烈建议把 Windows 的后缀显示给打开，不然你根本不知道自己到底在编辑什么文件。  
![](https://s2.ax1x.com/2019/09/01/nSe1X9.png)   

在 `config` 文件里写上一行就行：  
`ProxyCommand "C:\Program Files\Git\mingw64\bin\connect.exe" -S 127.0.0.1:1082 %h %p`   

这里 git 的安装路径和后面的代理自己看着填，不要试着用相对路径，保证要吃亏。    
因为 `Program Files` 文件夹中间带一个空格，所以这里需要把整个路径给引号引起来。  
后面的代理的话，`-S` 指是 socks 代理，默认是 socks5，后面的 `%h %p` 意思是 Host 和 Port，必须得写上。  

以上这个写法是针对全局的，如果想只针对某个网站的话，就这么写：    
```
Host github.com
  ProxyCommand "C:\Program Files\Git\mingw64\bin\connect.exe" -S 127.0.0.1:1082 %h %p

Host gitlab.com
  ProxyCommand "C:\Program Files\Git\mingw64\bin\connect.exe" -S 127.0.0.1:1082 %h %p
```
接下来可以先初步尝试一下，找个地方随便 clone 个仓库试试。比如 `git clone git@github.com:nodejs/node.git`，nodejs 的官方库，clone过来有800MB+，作为速度测试还是蛮可以的。
如图是我的情况，速度可以达到 7 MiB/s，比不开代理的 SSH 快了几百倍。   
clone 速度普遍在 5 MiB/s 以上：  
![](https://s2.ax1x.com/2019/09/01/nSMjmQ.png)  
push 速度还是比较慢的，只有 120 KiB/s 左右：  
![](https://s2.ax1x.com/2019/09/01/nSQjgK.png)     